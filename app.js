var mysql = require("mysql");
var path = require("path");
var express = require("express");

var pool = mysql.createPool({
    host: "localhost",
    port: "3306",
    user: "root",
    password: "12571",
    database: "books",
    connectionLimit: 4
});

const GET_ALL_BOOKS_WHERE_TITLE_LIKE_AS = "select author_lastname, author_firstname, title, cover_thumbnail from books "
+ "where title like ";
const GET_ALL_BOOKS_WHERE_AUTHOR_AS = "select author_lastname, author_firstname, title, cover_thumbnail from books "
    + "where  author_lastname or author_firstname like ?";

var app = express();

app.get("/api/books/title/:title_like", function(req, res){
    console.info(req);
    console.info(req.params.title_like);
    pool.getConnection(function(err, connection){
        if(err){
            res.status(400).send(JSON.stringify(err));
            return;
        }
        var title_like = " '%" + req.params.title_like + "%'";
        console.info(req.params.title_like);
        connection.query(GET_ALL_BOOKS_WHERE_TITLE_LIKE_AS + title_like ,[ ],function(err, results){
            connection.release();
            if(err){
                res.status(400).send(JSON.stringify(err));
                return;
            }
            console.info(results);
            res.json(results);
        });
    });
});

app.get("/api/books/author/:author_like", function(req, res){
    console.info(req);

    pool.getConnection(function(err, connection){
        if(err){
            res.status(400).send(JSON.stringify(err));
            return;
        }
        
        var author_like = "%" + req.params.author_like + "%";
        connection.query(GET_ALL_BOOKS_WHERE_AUTHOR_AS + author_like, [], function(err, results){
            connection.release();
            if(err){
                res.status(400).send(JSON.stringify(err));
                return;
            }
            console.info(results);
            res.json(results);
        });
    });
});

app.use("/bower_components", express.static(path.join(__dirname, "bower_components")));
app.use(express.static(__dirname + "/public"));

app.listen(3000, function () {
    console.info("App Server started on port 3000");
});

