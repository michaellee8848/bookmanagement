/**
 * Created by Michael on 2016/7/26.
 */
(function(){
    angular
        .module("BookApp", [])
        .controller("BookCtrl", BookCtrl)
        .service("DBService", DBService);

    BookCtrl.$inject = ["$http", "DBService"];
    DBService.$inject = ["$q", "$http"];
    
    function  BookCtrl($http, DBService) {
        var vm = this;
        vm.books = [];
        vm.book = {};
        vm.book.title = "";
        vm.book.lastname = "";
        vm.book.firstname = "";
        vm.book.thumbnail = null;
        vm.status = {
            message: "",
            code: 0
        };

        vm.searchByTitle = function () {
            console.info("searchByTitle ");
            console.info($http);
            $http.get("/api/books/title/" + vm.title)
                .then(function (result) {
                    vm.books = result.data;
                }).catch(function (error) {
                vm.err = error;
            });
        };
        vm.searchByAuthor = function () {
            $http.get("api/books/author/" + vm.lastname || vm. firstname)
                .then(function (result) {
                    vm.results = result.data;
                    console.info(vm.results)
                }).catch(function (error) {
                vm.err = error;
            });
        };
    }

    function DBService($q, $http) {
        this.saveBook = function () {
            var defer = $q.defer();

            $http.post("/api/book/save", vm.book)
                .then(function (result) {
                    console.info(result);
                    vm.status.message = "Edited successfully";
                    vm.status.code = 202;
                    defer.resolve(result);
                })
                .catch(function () {
                    vm.status.message = "an error occured";
                    vm.status.code = 400;
                    defer.reject();
                });
            return defer.promise;
        }

    }

})();